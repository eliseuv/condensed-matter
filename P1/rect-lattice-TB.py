import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
import matplotlib.ticker as plticker

# Parameters

# Rectangular lattice
a = 1.0
b = 1.5

# Energies
delta = 0.5
# Hopping
t_a = 0.8
t_b = 0.4
t_p = 1.0

# Energies calculated
def E_A(k_x, k_y):
    return -2.0*(t_a*np.cos(k_x*a) + t_b*np.cos(k_y*b))

def E_B(k_x, k_y):
    return E_A(k_x, k_y) + delta

def t_k(k_x, k_y):
    return -2.0*t_p*(np.cos(0.5*k_x*a) + np.cos(0.5*k_y*b))

# Dispersion
def epsilon(k_x, k_y):
    A = E_A(k_x, k_y)+E_B(k_x, k_y)
    B = np.sqrt(A**2 + 4.0*t_k(k_x, k_y)**2)
    return np.array([0.5*(A + B), 0.5*(A - B)])

# Plot parameters
k_limit = (1.0)*np.pi
n_k = 50
k_x = np.linspace(-k_limit, k_limit, n_k)
k_y = np.linspace(-k_limit, k_limit, n_k)
K_X, K_Y = np.meshgrid(k_x, k_y)
[eps1, eps2] = epsilon(K_X, K_Y)

# Plot

fig = plt.figure()
ax = plt.axes(projection='3d')
ax.set_aspect('auto')
ax.set_title('Tight Binding Energies for Rectangular Lattice', fontsize=16)
ax.set_xlabel('$k_x$')
ax.set_ylabel('$k_y$')
ax.set_zlabel('$\epsilon$')

ax.xaxis.set_major_formatter(plticker.FormatStrFormatter('%g $\pi$'))
ax.xaxis.set_major_locator(plticker.MultipleLocator(base=0.5))
ax.yaxis.set_major_formatter(plticker.FormatStrFormatter('%g $\pi$'))
ax.yaxis.set_major_locator(plticker.MultipleLocator(base=0.5))

# ax.contour3D(k_x, k_y, eps1, 100, cmap='binary')
# ax.contour3D(k_x, k_y, eps2, 100, cmap='binary')
ax.plot_surface(K_X/np.pi, K_Y/np.pi, eps1)
ax.plot_surface(K_X/np.pi, K_Y/np.pi, eps2)

plt.show()